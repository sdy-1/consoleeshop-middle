﻿using System;
using System.Linq;
using OnlineStoreConsoleUI.Controllers;
using OnlineStoreConsoleUI.Models;
using OnlineStoreDomain.Repositories;

namespace OnlineStoreConsoleUI.Commands
{
    public class ChangeOrderStatusCommand : CommandBase
    {
        public override string Name => "change status";
        public override string Description => "Change order status of a customer's order";

        public override Controller Execute<T>(T controller)
        {
            if (!(controller is AdminController adminController))
                return controller;

            Customer customer;
            while (true)
            {
                Console.WriteLine("Enter a customer's login");
                var login = Console.ReadLine();

                customer = CustomerRepository.Customers.FirstOrDefault(c => c.Login == login);

                if (customer is { })
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There is no user with such login");
                Console.ResetColor();
            }

            var placedOrders = customer.PlacedOrders;
            Console.WriteLine("Customer orders: ");
            for (int i = 0; i < placedOrders.Count(); i++)
            {
                Console.WriteLine($"{i + 1}. {placedOrders.ElementAt(i)} - {placedOrders.ElementAt(i).Status}");
            }

            Console.WriteLine("Enter a number of order you want to change");
            int key;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out key) && key >= 1 && key <= placedOrders.Count())
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Wrong input");
                Console.ResetColor();
            }

            var order = placedOrders.ElementAt(key - 1);
            ChangeStatus(order);

            return adminController;
        }

        private void ChangeStatus(Order order)
        {
            Console.WriteLine("Choose new category of a product");
            foreach (var categoryName in Enum.GetNames(typeof(OrderStatus)))
            {
                Console.WriteLine(categoryName);
            }

            OrderStatus status;
            while (true)
            {
                if (Enum.TryParse(Console.ReadLine(), out status))
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("This category does not exist");
                Console.ResetColor();
            }

            order.Status = status;
        }
    }
}