﻿using System;
using OnlineStoreConsoleUI.Repositories;

namespace OnlineStoreConsoleUI.Models
{
    public class Product
    {
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public Category Category { get; set; }
        
        public string Description { get; set; }

        public Product(string name, decimal price, string description, Category category)
        {
            _ = name ?? throw new ArgumentNullException(nameof(name));
            _ = description ?? throw new ArgumentNullException(nameof(description));
            
            Name = name;
            Price = price;
            Description = description;
            Category = category;
            
            ProductRepository.AddProduct(this);
        }

        public override string ToString() =>
            $"{Name} - {Price} - {Category} - {Description}";
    }
}